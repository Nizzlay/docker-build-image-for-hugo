FROM alpine:3.9
MAINTAINER Niels Gouman
ARG HUGO_VERSION="0.83.1"

# Install packages
RUN apk update && \
	apk upgrade && \
	apk --no-cache add \
	# Required for AWS-Cli
	python2 py2-pip \
	# Required for Hugo
	py2-pygments wget bash libc6-compat libstdc++ libgcc git curl \
	#  Required for HTML-Proofer
	ruby ruby-dev ruby-nokogiri build-base libxml2 zlib zlib-dev
# Python packages
RUN	pip --no-cache-dir install --upgrade \
		pip awscli
# Ruby packages
RUN gem install \
	json etc html-proofer --no-document
# Hugo
RUN wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz
RUN tar xzf hugo*.tar.gz -C /tmp && \
	mv /tmp/hugo /usr/sbin/hugo
# Cleanup
RUN rm -rf /tmp/*
RUN apk del \
	py2-pip wget \
	ruby-dev build-base zlib-dev